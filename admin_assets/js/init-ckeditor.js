$(document).ready(function(){
	CKEDITOR.replace('add_news', {
	      width: 600,
	      height: 300,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	
	CKEDITOR.replace('product_desc', {
	      width: 480,
	      height: 300,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	CKEDITOR.replace('varieties_desc', {
	      width: 480,
	      height: 300,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	CKEDITOR.replace('season_desc', {
	      width: 480,
	      height: 300,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	CKEDITOR.replace('management_desc', {
	      width: 480,
	      height: 300,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	CKEDITOR.replace('cultivation_desc', {
	      width: 480,
	      height: 300,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	
	CKEDITOR.replace('cat_terms', {
	      width: 650,
	      height: 250,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	CKEDITOR.replace('cat', {
	      width: 650,
	      height: 250,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
	CKEDITOR.replace('sub_cat', {
	      width: 650,
	      height: 250,
	      resize_dir: 'both',
	      resize_minWidth: 200,
	      resize_minHeight: 300,
	      resize_maxWidth: 800
	    });
}); 
