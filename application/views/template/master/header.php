
	<div class="page-loader">
		<div class="loader">
			<div class="blobs">
				<div class="blob-center"></div>
				<div class="blob"></div>
				<div class="blob"></div>
				<div class="blob"></div>
				<div class="blob"></div>
				<div class="blob"></div>
				<div class="blob"></div>
			</div>
			<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
				<defs>
					<filter id="goo">
						<feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
						<feColorMatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
						<feBlend in="SourceGraphic" in2="goo" />
					</filter>
				</defs>
			</svg>
		</div>
	</div>
	<div id="main_content">
	<header class="site-header header_trans-fixed" data-top="992">
			<div class="container">
				<div class="header-inner">
					<div class="toggle-menu"><span class="bar"></span>  <span class="bar"></span>  <span class="bar"></span>
					</div>
					<div class="site-mobile-logo">
						<a href="<?php echo base_url('home');?>" class="logo">
							<img src="<?php echo base_url()?>assets/img/logo.png" alt="site logo" class="main-logo">
							<img src="<?php echo base_url()?>assets/img/logo.png" alt="site logo" class="sticky-logo">
						</a>
					</div>
					<nav class="site-nav">
						<div class="close-menu"><span>Close</span>  <i class="ei ei-icon_close"></i>
						</div>
						<div class="site-logo">
							<a href="<?php echo base_url('home');?>" class="logo">
								<img src="<?php echo base_url()?>assets/img/logo.png" alt="site logo" class="main-logo">
								<img src="<?php echo base_url()?>assets/img/logo.png" alt="site logo" class="sticky-logo">
							</a>
						</div>
						<div class="menu-wrapper" data-top="992">
							<ul class="site-main-menu" >
								<li><a href="<?php echo base_url('home');?>">Home</a></li>
								<li><a href="<?php echo base_url('about_us');?>">About</a></li>
								<li class="menu-item-has-children"><a href="#">Internship</a>
									<ul class="sub-menu">
										
										<li><a href="<?php echo base_url('machine_learning');?>">Machine learning</a>
										</li>
										<li><a href="<?php echo base_url('web_technology');?>">Web Technology</a>
										</li>
										<li><a href="<?php echo base_url('java');?>">Java Programming</a></li>
										<li><a href="<?php echo base_url('python');?>">Python</a>
										</li>
										<li><a href="<?php echo base_url('net');?>">.NET</a>
										</li>
										<li><a href="<?php echo base_url('selenium_testing');?>">Selenium Testing</a>
										</li>
										<li><a href="<?php echo base_url('artificial_intelligence');?>">Artificial Intelliegence</a>
										</li>
										<li><a href="<?php echo base_url('digital_marketing_intern');?>">Digital Marketing</a>
										</li>
										<li><a href="<?php echo base_url('data_science');?>">Data Science</a>
										</li>
										<li><a href="<?php echo base_url('aws');?>">AWS</a>
										</li>
									</ul>
								</li>

								<li class="menu-item-has-children"><a href="#">Services</a>
									<ul class="sub-menu">
										
										<li><a href="<?php echo base_url('mobile_development');?>">Mobile App Development</a>
										</li>
										<li><a href="<?php echo base_url('software_development');?>">Software Development</a>
										</li>
										<li><a href="<?php echo base_url('web_development');?>">Web Development</a>
										</li>
										<li><a href="<?php echo base_url('e_commerce');?>">E Commerce</a>
										</li>

										<li><a href="<?php echo base_url('digital_marketing');?>">Digital Marketing</a>
										</li>
										<li><a href="<?php echo base_url('website_design');?>">Website Design</a>
										</li>
										<li><a href="<?php echo base_url('resource_fulfillment');?>">Resource fulfillment</a>
										</li>
										<li><a href="<?php echo base_url('technology_consulting');?>">Technology Consulting</a></li>
										
									
									</ul>
								</li>
								<!-- <li ><a href="<?php echo base_url('career');?>">Career</a> -->
									
								
								<li><a href="<?php echo base_url('home/contact');?>">Contact Us</a>
								</li>
							</ul>
							<div class="nav-right"><a href="#" class="nav-btn"> +91 7989331018</a>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</header>
	</div>
