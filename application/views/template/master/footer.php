		
			<section class="call-to-action action-padding">
			<div class="overlay-bg">
				<img src="media/background/ellipse.png" alt="bg">
			</div>
			<div class="container">
				<div class="row align-items-center justify-content-between">
					<div class="col-lg-7">
						<div class="action-content style-two wow pixFadeUp">
							<h2 class="title">Get update from anywheres<br>& subscribe us to get more info</h2>
						</div>
					</div><form action="<?php echo base_url('subscribe');?>"  method="post" enctype="multipart/form-data" class="newsletter-form wow pixFadeUp" data-pixsaas="newsletter-subscribe">
                            <div class="newsletter-inner">
                                <input type="email" name="email" class="form-control" id="newsletter-form-email" placeholder="Enter your Email" required>
                                <button type="submit" name="submit" id="newsletter-submit"  class="newsletter-submit" style="background: #fa7070;"><span >Subscribe</span>  <i class="fas fa-spinner fa-spin"></i>
                                </button>
                            </div>
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                            <div class="clearfix"></div>
                            <div class="form-result alert">
                                <div class="content"></div>
                            </div>
                        </form>
				</div>
			</div>
		</section>
		<footer id="footer">
			<div class="container">
				<div class="footer-inner wow pixFadeUp">
					<div class="row">
						<div class="col-lg-3 col-md-5">
							<div class="widget footer-widget">
								<h3 class="widget-title">Company</h3>
								<ul class="footer-menu">
									<li><a href="<?php echo base_url('home');?>">Home</a>
									</li>
									<li><a href="#">Internship</a>
									</li>
									
									<li><a href="<?php echo base_url('about_us');?>">About Us</a>
									</li>
									<li><a href="<?php echo base_url('contact');?>">Get In Touch</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-5">
							<div class="widget footer-widget">
								<h3 class="widget-title">Internship</h3>
								<ul class="footer-menu">
									<li><a href="<?php echo base_url('machine_learning');?>">Machine learning</a>
										</li>
										<li><a href="<?php echo base_url('web_technology');?>">Web Technology</a>
										</li>
										<li><a href="<?php echo base_url('java');?>">Java Programming</a></li>
										<li><a href="<?php echo base_url('python');?>">Python</a>
										</li>
										<li><a href="<?php echo base_url('net');?>">.NET</a>
										</li>
										<li><a href="<?php echo base_url('selenium_testing');?>">Selenium Testing</a>
										</li>
										<li><a href="<?php echo base_url('artificial_intelligence');?>">Artificial Intelliegence</a>
										</li>
										<li><a href="<?php echo base_url('digital_marketing_intern');?>">Digital Marketing</a>
										</li>
										<li><a href="<?php echo base_url('data_science');?>">Data Science</a>
										</li>
										<li><a href="<?php echo base_url('aws');?>">AWS</a>
										</li>
									
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-5">
							<div class="widget footer-widget">
								<h3 class="widget-title">Services</h3>
								<ul class="footer-menu">
										<li><a href="<?php echo base_url('mobile_development');?>">Mobile App Development</a>
										</li>
										<li><a href="<?php echo base_url('software_development');?>">Software Development</a>
										</li>
										<li><a href="<?php echo base_url('web_development');?>">Web Development</a>
										</li>
										<li><a href="<?php echo base_url('e_commerce');?>">E Commerce</a>
										</li>
										
										<li><a href="<?php echo base_url('digital_marketing');?>">Digital Marketing</a>
										</li>
										<li><a href="<?php echo base_url('website_design');?>">Website Design</a>
										</li>
										<li><a href="<?php echo base_url('resource_fulfillment');?>">Resource fulfillment</a>
										</li>
										<li><a href="<?php echo base_url('technology_consulting');?>">Technology Consulting</a></li>
									
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-5">
							<div class="widget footer-widget">
								<h3 class="widget-title">Our Address</h3>
								<p>7-1-214/4,4th floor, Dk Road, Ameerpet, Hyderabad, Telangana 500016</p>
								<ul class="footer-social-link">
									<li><a href="https://www.facebook.com/NextKlick"><i class="fab fa-facebook-f"></i></a>
									</li>
									<li><a href="https://twitter.com/NEXTCLICK1"><i class="fab fa-twitter"></i></a>
									</li>
									<li><a href="#"><i class="fab fa-google-plus-g"></i></a>
									</li>
									<li><a href="https://in.linkedin.com/company/nextclick-in"><i class="fab fa-linkedin-in"></i></a>
									</li>
								</ul>

							</div>
						</div>

					</div>
				</div>
				<div class="site-info">
					<div class="copyright">
						<p>© 2020 All Rights Reserved Design by <a href="#" target="_blank">*****</a>
						</p>
					</div>
					
				</div>
			</div>
		</footer>