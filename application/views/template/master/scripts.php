<script src="<?php echo base_url();?>dependencies/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/swiper/js/swiper.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/jquery.appear/jquery.appear.js"></script>
	<script src="<?php echo base_url();?>dependencies/wow/js/wow.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/countUp.js/countUp.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/jquery.parallax-scroll/js/jquery.parallax-scroll.js"></script>
	<script src="<?php echo base_url();?>dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url();?>dependencies/gmap3/js/gmap3.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDk2HrmqE4sWSei0XdKGbOMOHN3Mm2Bf-M&amp;ver=2.1.6"></script>
	<script src="<?php echo base_url();?>assets/js/header.js"></script>
	<script src="<?php echo base_url();?>assets/js/app.js"></script>

	
						

<!--Start of Tawk.to Script-->
<script type="text/javascript">
	
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f61c95df0e7167d0010be16/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
