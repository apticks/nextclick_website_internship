<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="mask-icon" href="assets/img/fav/safari-pinned-tab.svg" color="#fa7070">
    <meta name="msapplication-TileColor" content="#fa7070">
    <meta name="theme-color" content="#fa7070">
    <title>Nextclick</title>
	<?php $this->load->view('template/master/topcss');?>
</head>

<body id="home-version-1" class="home-version-3" data-style="default">
    <!-- right up icon start -->
    <!-- 
<a href="#main_content" data-type="section-switch" class="return-to-top"><i class="fa fa-chevron-up"></i></a> -->
<!-- right up icon end -->
<!-- navbar area start -->
	<?php $this->load->view('template/master/header');?>
<!-- navbar area end -->

<!-- Content area start -->
	<?php $this->load->view($content);?>
<!-- Content area end -->

<!-- footer area start -->
	<?php $this->load->view('template/master/footer');?>
<!-- footer area end -->


  <a  class="whats-app" href="https://api.whatsapp.com/send?phone=917989331018&text=Hi%20There!" target="_blank">
        <i class="fab fa-whatsapp my-float"></i>
    </a>

<?php $this->load->view('template/master/scripts');?>
</body>
</html>
<style type="text/css">
    .whats-app {
    position: fixed;
    width: 60px;
    height: 60px;
    bottom: 22px;
    left: 15px;
    background-color: #25d366;
    color: #FFF;
    border-radius: 50px;
    text-align: center;
    font-size: 30px;
    box-shadow: 2px 2px 3px #999;
    z-index: 100;
}
.my-float {
    margin-top: 16px;
}
</style>