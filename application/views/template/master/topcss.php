<link rel="stylesheet" href="<?php echo base_url();?>dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>dependencies/fontawesome/css/all.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>dependencies/swiper/css/swiper.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>dependencies/wow/css/animate.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>dependencies/magnific-popup/css/magnific-popup.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>dependencies/components-elegant-icons/css/elegant-icons.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>dependencies/simple-line-icons/css/simple-line-icons.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">