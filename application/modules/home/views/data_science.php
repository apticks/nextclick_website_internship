 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Data Science</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Data Science</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/data_internal.jpg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div><a href="#"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">Data Science</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Data Science training Program makes you proficient in tools and systems used by Data Science Professionals. It includes training on Statistics, Machine Learning, Data Science, Python,. Gain hands-on exposure to key technologies including R, Python, Tableau, Hadoop, and Spark. Become an expert Data Scientist today.</p>

                            <p class="wow pixFadeUp" data-wow-delay="0.3s"> At the end of the course we will be discussing various practical use cases of Data Science to enhance your learning experience. Next Click Data Science Certification Training course is also a gateway towards your Data Science career.</p>
                            <a href="<?php echo base_url('home/contact');?>" class="pix-btn btn-two wow fadeInUp" data-wow-delay="0.7s">Hurry Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp"> Getting Started With Data Science And Recommence Systems</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                          <li>Data Science Overview</li>
    <li>Reasons to use Data Science</li>
    <li>Project Lifecycle</li>
    <li> Data Acquirement</li>
    <li>Evaluation of Input Data</li>
    <li> Transforming Data</li>
    <li> Statistical and analytical methods to work with data</li>
    <li> Machine Learning basics</li>
    <li>Introduction to Recommender systems</li> 
    <li> Apache Mahout Overview</li> 
 </div>
                </div>
            </div>
        </section>
        <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp"> Reasons To Use, Project Lifecycle</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                          <li>What is Data Science?</li>
    <li>What Kind of Problems can you solve?</li>
    <li> Data Science Project Life Cycle⦁ </li>
    <li> Data Science-Basic Principles</li>
    <li>Data Acquisition</li>
    <li>Data Collection</li>
    <li>Understanding Data- Attributes in a Data, Different types of Variables</li>
    <li>Build the Variable type Hierarchy</li>
    <li>Two Dimensional Problem</li> 
    <li>Co-relation b/w the Variables- explain using Paint Tool</li>
<li>Outliers, Outlier Treatment</li>
<li> Boxplot, How to Draw a Boxplot</li> 
 </div>
                </div>
            </div>
        </section>
        
      