 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">.NET
</h1>
                    <ul class="bradcurmed">
                        <li><a href="index.html" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>.NET</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                        <img src="<?php echo base_url();?>assets/img/courses/net_internal_image.jpg" class="elm-one wow pixFade" alt="informes">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">.NET</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">This .NET Programming training course provides hands-on experience creating software for
Microsoft's .NET (Windows platform) using the Visual Studio development environment.
Starting with the most fundamental elements of computer programming, the training evolves
to leverage development techniques sufficient to produce a complete web application including
the user interface, business logic and data access layers.You learn how to write code using C#;
create ASP.NET Web applications and process Web forms and build SQL Server databases and
access them using ADO.NET.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         