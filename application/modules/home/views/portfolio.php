<section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Portfolio</h2>
          <p>A portfolio is a compilation of materials that exemplifies your beliefs, skills, qualifications, education, training and experiences. It provides insight into your personality and work ethic.</p>
        </div>


        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
<?php foreach ($portfolio as $portfolio):?>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
                  <img src="<?php echo base_url()?>uploads/portfolio_image/portfolio_<?php echo $portfolio['id'];?>.jpg?<?php echo time();?>">
              <div class="portfolio-info">
                <h4><?php echo $portfolio['title'];?></h4>
                <div class="portfolio-links">
                  <a href="<?php echo base_url()?>uploads/portfolio_image/portfolio_<?php echo $portfolio['id'];?>.jpg?<?php echo time();?>" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="<?php echo base_url();?>portfolio_details?id=<?php echo $portfolio['id'];?>" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox" type="portfolio" title="Portfolio Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>
<?php endforeach;?>
         
         
          

         

          

          

          

        </div>

      </div>
    </section>