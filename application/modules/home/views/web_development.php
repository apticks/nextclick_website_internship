 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title"> Web Development</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Web Development</h1></li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/web_development.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About Web Development!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Our Website Development comes with intuitive and creative design thought, and cutting edge technology. We take-up challenges and bring your dream website to life as visually stunning design. Those days are gone when website were merely a brochure uploaded online. It's an extension of your brand-identity today and are apparatuses to increase business online. Whether you need to build a website from scratch, redesign an existing website or develop a mobile website, we can execute all this and more. We help you in designing a creative website with responsive design that is flexible across all the smart devices and provides the visitor a rich user experience, no matter how large or small their display.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">More About Web Development</h3>
                                </div>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">At Next Click, our Web Designers and Developers are dedicated to producing quality business websites. We offer a full Web Service, which begins with </p> 
                                </div>
                    </div>
                </div>
            </div>
        </section>
        
       <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp">Some of our Web Development feature includes:</h3>
                               
                            </div>
                               
                            <li>Responsive Website Development</li>
                            <li>User Experience Design</li>
                            <li>Customized web development</li>
                            <li>Brand Identity</li>
                            <li>Cross Browser Compatibility</li>
                            <li>W3C Validations for HTML and CSS.</li>
                            <li>Creative, Clean, Professional</li>
                            <li>Pixel perfect designing & HTML conversion</li>


                           </div>
                    </div>
                </div>
            </div>
        </section>
         
<br>
<br>