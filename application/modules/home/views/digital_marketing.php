 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title"> Digital Marketing
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li> Digital Marketing
</h1></li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/digital_marketing.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About Digital Marketing!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">As a Digital Marketing company, Next Click provides entire services in promotions of products and brands in the world of Internet Marketing to reach customers. We provide services of search engine optimization (SEO), search engine marketing (SEM), Social Media Optimizations and many more.</p>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Our expert team of Internet Marketing has created marketing products, brands, and services to increase sales and marketing.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">Search Engine Optimization (SEO)</h3>
                                </div>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">
Search Engine Optimization (SEO) is a process to increase the number of visitors and give visibility of your website or a web page by obtaining high rankings on search engine pages through natural or un-paid (organic) search results. </p> 
                                 
                               
                            

                           </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">Search Engine Marketing (SEM)</h3>
                                  </div>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">

Search Engine Marketing (SEM) is a strategy of Internet Marketing which focuses to increase visibility of a website in search engine result pages (SERP) and Pay per Click (PPC) listings through optimization and advertising. 

                               
                          

                           </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">Social Media Optimization (SMO)</h3>
                                 </div>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">

Social Media Optimization (SMO) is a process of gaining traffic through various social media sites to increase the awareness of brands, products, or services. It includes RSS feeds, social networking sites, and social news, video and blog sites. 
</p> 
                                 
                               
                           

                           </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">Digital Content Marketing</h3>
                                </div>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">Content Marketing is a marketing technique publishes a relevant, valuable, and unique contents attract visitors and making them into a potential customer on various format. It is driven to improve your business or organization.</p>

                           </div>
                    </div>
                </div>
            </div>
        </section>
         
<br>
<br>