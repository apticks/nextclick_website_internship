
        
       
        <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">About Us
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/about_us.jpeg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">Its about Us!</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Next Click Info Solutions Private Limited provides global clients with a distinctive offer – the blend of Business Process Outsourcing and Superior Technology – in a single entity through our offshore development facility in India &USA. The outcome is better process mechanization and technical developments that can trim down operational expenses for your business. The NEXTCLICK is a Software Service company having presence in vast field of web services, Custom Application Development.!</p>

                            <p class="description wow pixFadeUp" data-wow-delay="0.4s">NEXTCLICK came into existence from 22nd Aug 2019, by acquiring Innovative Software’s. Starting with inorganic growth is a well thought out step that forges our strategy to grow by access to an already proven talent pool, international presence and in-house expertise of Innovative Software’s.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">Our Mission</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Our mission is to provide innovative software solutions for excellence and enterprise compliance.!</p>

                           
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeLeft">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/our_mision.jpg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/our_vision.jpg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">Our Vission</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Our vision is to become businesses’ first choice when it comes to software development and maintenance. To accomplish this, we always try to exceed our client’s expectations. NEXT CLICK strives to build lasting partnerships and ensures client satisfaction. !</p>

                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimonials-three">
            <div class="container">
                
                <div id="testimonial-wrapper-three">
                    <div class="swiper-container wow pixFadeUp" data-wow-delay="0.5s" id="testimonial-slider-three" data-speed="700" data-autoplay="5000" data-perpage="1" data-space="90" data-breakpoints='{"768": {"spaceBetween": 50}, "640": {"spaceBetween": 30}}'>
                        <div class="swiper-wrapper">
                          <!--   <div class="swiper-slide"> -->
                                <div class="testimonial-three">
                                    <div class="avatar">
                                        <img src="<?php echo base_url();?>assets/img/courses/founder.jpeg" alt="testimonial" style="
    padding: 72px;
">
                                    </div>
                                    <div class="testimonial-content">
                                        <div class="bio-info">
                                            <h3 class="name">Vinay Sagar Enugula</h3><span class="job">CEO and founder</span>
                                        </div>
                                        <p>A prominent figure who has started doing business at an early age of his life. He began intelligently with the  business and focused on keeping up with the local requirements at an individual and company level.!</p>
                                    </div>
                                </div>
                          <!--   </div> -->
                        </div>
                    </div>
                    <div class="quote">
                        <img src="media/testimonial/quote2.png" alt="">
                    </div>
                </div>
            </div>
        </section>