     <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">PYTHON
</h1>
                    <ul class="bradcurmed">
                        <li><a href="index.html" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Python</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                         <img src="<?php echo base_url();?>assets/img/courses/python_internal_image.jpg" class="elm-one wow pixFade" alt="informes">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Python</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Nextclick Offers Best Python Online & offline Training with Python Experts. Become Master in
Python Concepts like Basic Python Syntax, Language Components, Collections, Functions,
Modules, Exceptions, Classes & Advanced Concepts in Python with our Practical Classes. We
Guarantee for your Python Online Training Success with Certification. We focused on 100%
Practical & Certification Oriented Python Courses with Placements for our Students. Through
this Python Certification training, you can simply clear the certifications Certified Entry-Level
Python Programmer, Certified Professional in Python Programming, and Certified Expert in
Python Programming. Python Online training course provides multiple hands-on projects and
live experience.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp">Description</h3>
                               
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.4s">Python is an interpreted, object-oriented, high-level programming language with dynamic
semantics. Its high-level built in data structures, combined with dynamic typing and dynamic
binding, make it very attractive for Rapid Application Development, as well as for use as a
scripting or glue language to connect existing components together. Python's simple, easy to
learn syntax emphasizes readability and therefore reduces the cost of program maintenance.
Python supports modules and packages, which encourages program modularity and code
reuse. The Python interpreter and the extensive standard library are available in source or
binary form without charge for all major platforms, and can be freely distributed.
</p> <p class="description wow pixFadeUp" data-wow-delay="0.4s">
Often, programmers fall in love with Python because of the increased productivity it provides.
Since there is no compilation step, the edit-test-debug cycle is incredibly fast. Debugging
Python programs is easy: a bug or bad input will never cause a segmentation fault. Instead,
when the interpreter discovers an error, it raises an exception. When the program doesn't
catch the exception, the interpreter prints a stack trace. A source level debugger allows
inspection of local and global variables, evaluation of arbitrary expressions, setting breakpoints,
stepping through the code a line at a time, and so on. The debugger is written in Python itself,
testifying to Python's introspective power. On the other hand, often the quickest way to debug
a program is to add a few print statements to the source: the fast edit-test-debug cycle makes
this simple approach very effective.
</p>
                           </div>
                    </div>
                  <!--   <div class="col-lg-5">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="media/about/1.jpg" alt="about">
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <section class="newsletter-two" id="newsletter">
            <div class="container">
                <div class="newsletter-content-two text-center wow pixFadeUp">
                    <h2 class="title">Feature!!</h2>
                    
                        <li>Uses an elegant syntax, making the programs you write easier to read</li>
                        <li>Is easily extended by adding new modules implemented in a compiled language such as C or C++.</li>
                        <li>Can also be embedded into an application to provide a programmable interface.</li>
                        <li>Runs anywhere, including Mac OS X, Windows, Linux, and Unix, with unofficial builds also available for Android and iOS.</li>
                        <li>Is free software in two senses. It doesn't cost anything to download or use Python, or to
include it in your application.</li>
                        <li>Python can also be freely modified and re-distributed
because while the language is copyrighted it's available under an open-source license.
</li>
<li>Python contains advanced programming features such as generators and list
comprehensions.</li>
<li>Python's automatic memory management frees you from having to manually allocate and
free memory in your code<</li>

                </div>
              
            </div>
            <div class="scroll-circle wow pixFadeUp">
                <img src="media/background/circle12.png" data-parallax='{"y" : -230}' alt="circle6">
            </div>
        </section>
        
         <br>
         <br>
         <br>
         <br>
         <br>
         <br>