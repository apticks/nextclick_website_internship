 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Resource Fulfillment
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Resource Fulfillment</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/resource.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About Resource Fulfillment!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Whether you are a small business or a large corporate, the perception of general Resource Fulfilment has become a leading HR trend worldwide and has emerged rapidly to address various client requirements. This is where we can help.</p>
                              <p class="description wow pixFadeUp" data-wow-delay="0.4s">We have a workforce with just one mission to fulfil - finding the right people to meet your specific needs. You may need people with a certain level of experience in a particular domain, or in a specific location. No matter what you require, our qualified staffing services division will find people who will meet the unique needs of your firm.</p> 
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

           <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                     <h3 class="sub-title wow pixFadeUp">Description</h3> 
                </div>
                <div class="row">
                    <div class="col-lg-12">
                       
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">By providing strategic value-addition in the form of cost effective staffing solutions to maximize productivity and improve capacity, we achieve success in the satisfaction of our customers.</p>

                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">We understand, define, and solve specific staffing needs, thus allowing small and large organisations the time and freedom to focus on their core business. With NextKlick.info as your consulting partner, you can easily meet your short-term and long-term business objectives. </p>

                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Once we thoroughly understand your staffing requirements, we will be able to provide you the right people with the right skill set, attitude, and commitment that will help propel your organisation toward success.</p>
                                 </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                     <h3 class="sub-title wow pixFadeUp">Our staffing solutions include</h3> 
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">We understand that every company has its own culture, values, and expectations from its employees. The better we can understand your unique way of functioning, the easier it will be for us to tailor our search and narrow down the field of potential candidates for employment at your organisation.</p> 
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Take any successful business and you will find that it is powered by a strong workforce of committed and dedicated employees. Let us help you procure a winning team that will improve productivity, and increase revenues. </p>

                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Our strong presence (international-wise), helps ensure that we can find the perfect resources you need in any market where you do business.</p>
                    </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                     <h3 class="sub-title wow pixFadeUp">Let us be your partner in success.</h3> 
                </div>
            </div>
        </section>