 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Testing
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Testing</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/selenium_internal_image.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About Selenium!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Nextclick offers Selenium Training Course is curated by Industry Experts and it expansively
covers Selenium WebDriver, Selenium Grid, Selenium IDE, handling IFrames, Alerts and Modal
Dialog box. You will learn to use Selenium supported plugins such as TestNG Framework, Robot
Class, Cucumber, and Gherkin to control your automation environment. Get hands-on
experience on widely used automation frameworks such as Data-Driven Framework,
Keyword-Driven Framework, Hybrid Framework, and Behaviour Driven Development (BDD)
Framework. Throughout this online Instructor-led SeleniumTraining, you will be working on
real-life industry use cases.
</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp">Selenium Training is designed to help you become a successful Professional in Automation
Testing. Upon completion of the course, you will be able to:</h3>
                               
                            </div>
                            <li>Understand Selenium WebDriver</li>
                            <li>Identify web elements using various locators</li>
                            <li>Interact with web elements using Selenium WebDriver</li>
                            <li>Handle multiple browser windows, browser tabs and scrolling on a web page using
Selenium WebDriver</li>
<li>Group and sequence Tests, execute Parallel Tests, and generate Test Reports using
TestNG</li>
<li>Handle Alerts, Modal Dialog Box, IFrames, Dropdowns, and Multiple Select Operations
on web applications</li>
<li>Create your own Grid of nodes and web browsers for Parallel Testing</li>
<li>Utilize Robot Class to manage keyboard and mouse events on a web application</li>
<li>Learn to build a test case using Selenium</li>
</p> 
<div class="row">
 

</div>

                           </div>
                    </div>
                </div>
            </div>
        </section>