 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">AWS</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>AWS</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/aws_internal.jpg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div><a href="#"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">AWS</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">This session deals with the basics of cloud computing. Significant features of the cloud when compared with on-premises. You will get a good understanding of the difference between public, private and hybrid cloud environments. Discuss various cloud services like. </p>
                            <ul class="list-items-three wow pixFadeUp" data-wow-delay="0.5s">
                                <li>IAAS(Infrastructure As A Service)</li>
                                <li>PAAS(Platform As A Service)</li>
                                <li>SAAS(Software As A Service)</li>
                            </ul><a href="<?php echo base_url('home/contact');?>" class="pix-btn btn-three wow fadeInUp" data-wow-delay="0.7s">Hurry Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp"> Introduction to AWS Learning Objective:</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="wow pixFadeUp" data-wow-delay="0.3s">Amazon web service is one of the leading cloud providers available in the market. AWS provides a variety of services for your business and helps you get through digital transformation for the future. You will be learning the AWS global infrastructure like Regions, Availability Zones and Edge Locations. Overview of services provided by AWS such as:</p>
                        <li>Compute</li>
                        <li>Storage</li>
                        <li>Database</li>
                        <li>Networking</li>
                        <li>Security</li>
                        <li>Applications</li>
                    <p class="wow pixFadeUp" data-wow-delay="0.3s">End of this session you will be having a good understanding of cloud concepts and its services and the Impact of AWS in a cloud environment.</p> 
                    <h3 class="sub-title wow pixFadeUp"> Hands-on:</h3>
                     <div class="col-lg-12">
                        <li>Open AWS free tier account</li>
                        <li>Setup payment methods and billing preferences</li>
                        <li>Set alarm for free tier usage</li>
 </div>
 </div>
                </div>
            </div>
        </section>
          <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp"> Virtual Private Cloud Learning Objective:</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="wow pixFadeUp" data-wow-delay="0.3s">Logically isolated network devices (instances) from other AWS resources called VPC. You can provide your own private IPs for your instances. Your instances inside VPC will be interconnected together through VPC. Basic components of VPC to create a basic infrastructure is follows</p> 
                        <li>VPC</li>
                        <li>Subnets</li>
                        <li>Route Tables</li>
                        <li>Internet gateway</li>
                        
                         <p class="wow pixFadeUp" data-wow-delay="0.3s">End of this session you will have clarity of AWS regions, Availability Zones, and Edge locations. Also, you will learn CIDR concepts to get a better understanding of IP ranges.</p> 
                         <h3 class="sub-title wow pixFadeUp"> Hands-on:</h3>
                        <li> Create and configure VPC, Subnets, Internet gateway, and Route Tables</li>
                        <li>Create NAT Gateway, NAT instances, VPC Peering, Endpoints</li>
                        <li>Different between NACL and Security groups</li>

                         
 </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp"> EC2</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="wow pixFadeUp" data-wow-delay="0.3s">Learning Objective: EC2 provides a scalable computing capacity for your business. AWS has a wide range of instance family (type) include Memory-optimized, CPU optimized, Networking, etc. You can create instances using Amazon Machine Images, which is preconfigured by Amazon. AWS lets you create your own AMI and you can choose from any AMI available in Amazon Marketplace as well. Instances are available in many options:</p> 
                        <li>On-demand</li>
                        <li>Reserved</li>
                        <li>Spot Instance</li>
                        <li>Scheduled instance</li>
                        
                         <p class="wow pixFadeUp" data-wow-delay="0.3s">As a solution Architect, you will be able to give a solution for your client to choose the perfect compute service for their business needs.</p> 
                         <h3 class="sub-title wow pixFadeUp"> Hands-on:</h3>
                        <li>Create and launch different types of operating systems like Linux, Windows and connect them.</li>
                        <li>Create AMIs and Snapshots and launch instances using them.</li>
                        <li>Create additional Volume and attach it with your instance.</li>

                         
 </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">Storage Learning Objective</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="wow pixFadeUp" data-wow-delay="0.3s">There are many storage options available in AWS as per your needs from short term to long term. In Amazon S3 you can store an unlimited amount of data. S3, EBS, EFS, FSx, Glacier, and DeepGlacier are some major storage options available in AWS. In this session, you will be learning storage classes available in S3 and its features, cost, etc. You will get a brief knowledge of storage classes so that you can suggest your client more cost-effective. Storage classes are:</p> 
                        <li>Standard</li>
                        <li>Standard Infrequent Access</li>
                        <li>Standard Intelligent Tier</li>
                        <li>S3 Glacier</li>
                        <li>S3 Glacier Deep</li> 
                         <h3 class="sub-title wow pixFadeUp"> Hands-on:</h3>
                        <li>Create and attach EFS with instances</li>
                        <li>Create S3 and apply lifecycle policy and replicate the bucket into another bucket.</li>
                        <li>Create static Webhosting.</li>

                         
 </div>
                </div>
            </div>
        </section>
      