 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Mobile Development</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Mobile Development</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/mobile_app.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its Mobile Development!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Smart Phones are indeed necessary for today’s generation as the digital revolution is spreading its wings and smart phones are one of the most revolutionary products of all time. A mobile app has immense power to bring in more customers and set up a great connection with customers. It’s high time for companies to embrace this change and indulge mobile app in their business to step up their business game. Mobile app development is a perfect profitable deal. If you are curious and you are willing for a great success and profit on this platform. If yes, then you are on the right track and with the right people. Next Click is the one-stop destination for all your mobile app development needs including android app development, iOS app development and window application development.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
           <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                     <h3 class="sub-title wow pixFadeUp">Description</h3> 
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Next Click is the top leading mobile app development company in Hyderabad having the team of professionally qualified staff to fulfill all your business needs with perfection. We create the best mobile apps of any complexity or intensity. Next Click is working for years in the same field and having rich experience as well as smart tricks and tactics that will add the feather in one's cap and give you the best mobile app developing experience.</p> 
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Next Click is one of the fastest growing mobile app developing company having the 28+ happy and satisfied customers. Mobile app development is one of our expertise areas including Ecommerce, Multimedia, Hospitality, Health care, Education, Advertising, and many more. Next Click believes in empowering business with smart and unique strategies that make you ahead of your competitors.</p>

                        
                                 </div>
                </div>
            </div>
        </section>