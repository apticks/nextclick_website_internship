 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Web Technology
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>l" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Web Technology</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/web_technologies_internal_image.jpg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div><a href="#"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">Its about Web!</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Web technology refers to the means by which computers communicate with each other using markup languages and multimedia packages.!</p>
                            <ul class="list-items wow pixFadeUp" data-wow-delay="0.5s">
                                <li>HTML</li>
                                <li>CSS</li>
                                <li>Javascript</li>
                                <li>Bootstrap.</li>
                            </ul><a href="#" class="pix-btn btn-two wow fadeInUp" data-wow-delay="0.7s">Hurry Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">HTML</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">HTML stands for Hyper Text Markup Language. It is used to design web pages using
markup language. HTML is the combination of Hypertext and Markup language.
Hypertext defines the link between the web pages. Markup language is used to define
the text document within a tag which defines the structure of web pages. This language
is used to annotate (make notes for the computer) text so that a machine can
understand it and manipulate text accordingly. Most markup (e.g. HTML) languages are
human readable. Language uses tags to define what manipulation has to be done on
the text.

</p> 
 </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">Cascading Style Sheets</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Cascading Style Sheets, fondly referred to as CSS, is a simply designed language intended to
simplify the process of making web pages presentable. CSS allows you to apply styles to web
pages. More importantly, CSS enables you to do this independent of the HTML that makes up
each web page.

</p> 
 <p class="description wow pixFadeUp" data-wow-delay="0.4s">There are three types of CSS which are given below:
    <li>Inline CSS</li>
    <li>Internal or Embedded CSS</li>
    <li>External CSS</li>
</p> 
                    </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">JavaScript</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">JavaScript is a lightweight, cross-platform and interpreted scripting language. It is well-known
for the development of web pages, many non-browser environments also use it. JavaScript can
be used for Client-side developments as well as Server-side developments</p> 
 <p class="description wow pixFadeUp" data-wow-delay="0.4s">Features of JavaScript:
    <li>JavaScript was created in the first place for DOM manipulation. Earlier websites
were mostly static, after JS was created dynamic Web sites were made.
</li>
    <li>Functions in JS are objects. They may have properties and methods just like
another object. They can be passed as arguments in other functions.</li>
    <li>Can handle date and time.
</li>
<li>Performs Form Validation although the forms are created using HTML</li>
<li>No compiler needed.</li>
</p> 
 </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">Bootstrap </h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Bootstrap is a web framework that focuses on simplifying the development of informative web
pages (as opposed to web apps). The primary purpose of adding it to a web project is to apply
Bootstrap's choices of color, size, font and layout to that project. As such, the primary factor is
whether the developers in charge find those choices to their liking. Once added to a project,
Bootstrap provides basic style definitions for all HTML elements. The result is a uniform
appearance for prose, tables and form elements across web browsers. In addition, developers
can take advantage of CSS classes defined in Bootstrap to further customize the appearance of
their contents. For example, Bootstrap has provisioned for light- and dark-colored tables, page
headings, more prominent pull quotes, and text with a highlight.</p> 
</div>
                </div>
            </div>
        </section>
          <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">jQuery </h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">jQuery is an open source JavaScript library that simplifies the interactions between an
HTML/CSS document, or more precisely the Document Object Model (DOM), and JavaScript.
Elaborating the terms, jQuery simplifies HTML document traversing and manipulation, browser
event handling, DOM animations, Ajax interactions, and cross-browser JavaScript development.
jQuery is a JavaScript library designed to simplify HTML DOM tree traversal and manipulation,
as well as event handling, CSS animation, and Ajax. It is free, open-source software using the
permissive MIT License. As of May 2019, jQuery is used by 73% of the 10 million most popular
websites. Web analysis indicates that it is the most widely deployed JavaScript library by a large
margin, having 3 to 4 times more usage than any other JavaScript library.</p> 
</div>
                </div>
            </div>
        </section>