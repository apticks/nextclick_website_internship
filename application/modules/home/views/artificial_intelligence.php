 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Artificial Intelligence</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Artificial Intelligence</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-tax">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-video-wrapper">
                            <div class="popup-videos wow pixFadeRight">
                                <div class="video-thumbnail">
                                    <img src="<?php echo base_url();?>assets/img/courses/artificial_internal.jpg" alt="thumbnail"> <span class="dot-shape"><img src="media/about/dot.png" alt="saaspik"></span>
                                </div><a href="#"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tax-content">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp">Its about Artificial Intelligence!</h2>
                            </div>
                            <p class="wow pixFadeUp" data-wow-delay="0.3s">Get enrolled for the most demanding skill in the world. AI training will make your career a new height. We at Next Click provide you an excellent platform to learn and explore the subject from industry experts. We help students to dream high and achieve it.</p>
                            <a href="<?php echo base_url('home/contact');?>" class="pix-btn btn-three wow fadeInUp" data-wow-delay="0.7s">Hurry Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">Artificial Intelligence</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">Artificial Intelligence is such a happening concept these days. And people are showing extreme eagerness to take a course in AI. This intelligence shown by machines is something that has a wide application and has been able to carve a niche for itself. Although there are many company's that offer AI internship training in Hyderabad, you have to be extra cautious while choosing one for yourself. Choose a training company's that promises quality training.</p> 
 </div>
                </div>
            </div>
        </section>
         <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">AI (Artificial Intelligence) Internship Training in Hyderabad</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        
 <p class="description wow pixFadeUp" data-wow-delay="0.4s">
    <li>Module 1: Introduction to Data Science </li>
    <li>Module 2: Introduction to Python & Basics </li>
    <li>Module 3: Python Packages </li>
    <li>Module 4: Importing Data &Manipulating Data </li>
    <li>Module 5: Statistics Basics (Duration-11hrs)</li>
    <li>Module 6: Error Metrics (Duration-3hrs)</li>
    <li>Module 7(Machine Learning):Machine Learning, Supervised Learning, Unsupervised Learning, SVM, SVM Kernal</li>
    <li>Module 8(ARTIFICIAL INTELLIGENCE): AI Introduction</li>
    <li>Module 9(Deep Learning Algorithms): Introduction to NLP, Text to Features (Feature Engineering), Tasks of NLP
 </li>
</p> 
                    </div>
                </div>
            </div>
        </section>
         
        