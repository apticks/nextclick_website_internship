<section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Machine Learning
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Machine Learning</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/machine_learning_internal_image.jpeg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Machine learning</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Machine learning (ML) is the study of computer algorithms that improve automatically through
experience. It is seen as a subset of artificial intelligence. Machine learning algorithms build a
mathematical model based on sample data, known as "training data", in order to make
predictions or decisions without being explicitly programmed to do so. Machine learning
algorithms are used in a wide variety of applications, such as email filtering and computer
vision, where it is difficult or infeasible to develop conventional algorithms to perform the
needed tasks.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-three text-center">
                    <h3 class="sub-title wow pixFadeUp">Machine Learning Course Overview</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <p class="description wow pixFadeUp" data-wow-delay="0.4s">This Machine Learning online course offers an in-depth overview of Machine Learning topics
including working with real-time data, developing algorithms using supervised & unsupervised
learning, regression, classification, and time series modeling. Learn how to use Python in this
Machine Learning certification training to draw predictions from data
</p> 
 <p class="description wow pixFadeUp" data-wow-delay="0.4s">Machine Learning
Training Course with Machine Learning Certification from Experts. In this Machine Learning
Online Training, you will learn in-depth syllabus of Machine Learning Course which has
Introduction to Data Analytics, Introduction to R programming, Data Manipulation in R, Data
Import techniques, Exploratory data Analysis, Basics of Statistics & Linear & Logistic Regression,
Data Mining: Clustering techniques, Regression & Classification, Anova & Sentiment Analysis,
Data Mining: Decision Trees and Random Forest. Our Machine Learning Online Course syllabus
is designed to teach Machine Learning Course with practicals.

</p> 
                    </div>
                </div>
            </div>
        </section>