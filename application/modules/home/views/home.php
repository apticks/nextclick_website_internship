<?php if(!empty($this->session->flashdata('success'))){ ?>

    <script type='text/javascript'>
        window.alert('Successfully success!!!')
    </script>

    <?php }?>
    <section class="banner banner-three">
            <div class="container">
                <div class="banner-content-wrap-two">
                    <div class="banner-content text-center">
                        <h1 class="banner-title wow pixFadeUp" data-wow-delay="0.3s">DELIVERING<br>BEYOND YOUR EXPECTATION!!<br>To take your business to the next level.</h1>
                       <!--  <p class="description wow pixFadeUp" data-wow-delay="0.5s">To take your business to the next level.
                            </p> -->
                       <!--  <div class="banner-button-container"><a href="#" class="pxs-btn banner-btn wow pixFadeUp" data-wow-delay="0.6s">Free Trial</a>  <a href="https://www.youtube.com/watch?v=9No-FiEInLA" class="play-btn popup-video wow pixFadeUp" data-wow-delay="0.6s"><i class="ei ei-arrow_triangle-right"></i> Watch Video</a>
                        </div> -->
                    </div>
                   <div class="popup-videos-two wow fadeInUp">
                    <div class="video-thumbnail">
                        <img src="<?php echo base_url();?>assets/img/courses/intern_image.jpeg" alt="thumbnail">
                    </div>
                </div>
                </div>
            </div><!-- 
            <div class="bg-shape-inner">
                <div class="circle-shape wow pixFadeRight">
                    <img src="media/background/circle4.png" data-parallax='{"x" : -50}' alt="circle">
                </div>
                <div class="shape wow pixFadeLeft">
                    <img src="media/background/shape2.png" data-parallax='{"x" : 50}' alt="shape">
                </div>
            </div> -->
        </section>
          <br>
      <br> <br>
      <br> <br>
      <br> <br>
      <br><br>
      <br>
       <section class="featured-four-ab">
            <div class="container">
                <div class="section-title text-center">
                    <h3 class="sub-title wow pixFadeUp" style="font-weight: 1000">Our service</h3>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.5s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/android_devlopment.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('mobile_development');?>">Mobile Development</a></h3>
                                <p>Next Click is the top leading mobile app development company in Hyderabad having the team of professionally qualified staff with perfection.!</p><a href="<?php echo base_url('mobile_development');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a> 
                                <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                    <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.6s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/digital_marketing.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('digital_marketing');?>">Digital Marketing</a></h3>
                                <p>As a Digital Marketing company, Next Click provides entire services in promotions of products and brands in the world of Internet Marketing to reach customers.!</p><a href="<?php echo base_url('digital_marketing');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/techology_consulting.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('technology_consulting');?>">Technology Consulting</a></h3>
                                <p>Our consulting practice is built on the real-world operational experience of our transformation consultants and subject-matter experts.!</p><a href="<?php echo base_url('technology_consulting');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                     <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/resource_fulfillment_icon.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('resource_fulfillment');?>">Resource Fulfillment</a></h3>
                                <p>We understand, define, and solve specific staffing needs, thus allowing small and large organisations the time and freedom to focus on their core business. !</p><a href="<?php echo base_url('resource_fulfillment');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                      <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/web_development.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('software_development');?>">Software Development</a></h3>
                                <p>Next Click is a software development company that has enhanced in delivering productive software development services.!</p><a href="<?php echo base_url('software_development');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                      <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/web_development.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('web_development');?>">Web Development</a></h3>
                                <p>Our Website Development comes with intuitive and creative design thought, and cutting edge technology. !</p><a href="<?php echo base_url('web_development');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                      <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/ecommers.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('e_commerce');?>">E Commerce</a></h3>
                                <p>Next Click Info Solutions Pvt Ltd, is one of the leading Ecommerce development company in serving clients across the globe. !</p><a href="<?php echo base_url('e_commerce');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                      <div class="col-lg-3 col-md-6">
                        <div class="saaspik-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
                            <div class="saaspik-icon-box-icon">
                                <img src="<?php echo base_url();?>assets/img/courses/icons/icons/web_designing.jpg" alt="">
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="pixsass-icon-box-content">
                                <h3 class="pixsass-icon-box-title"><a href="<?php echo base_url('website_design');?>">Website Design</a></h3>
                                <p>At Next Click, we have an expert team of Web Development, Graphic Designers, Expert Logo Designers, and Content Developers who work for our clients.!</p><a href="<?php echo base_url('website_design');?>" class="more-btn"><i class="ei ei-arrow_right"></i></a>
                            </div>
                            <svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="blog-grid" class="featured-four">
            <div class="container">
                <div class="section-title color-two text-center">
                    <h3 class="sub-title wow pixFadeUp">Online & Offline</h3>
                    <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Check out our Courses</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.4s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('machine_learning');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/ml2.jpg"  alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                                <ul class="post-meta">
                                    
                                </ul>
                                <h3 class="entry-title"><a href="<?php echo base_url('machine_learning');?>">Machine learning</a></h3>
                              
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.6s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('web_technology');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/webtechnology.jpg"  alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                                <ul class="post-meta">
                                   
                                </ul>
                                <h3 class="entry-title"><a href="<?php echo base_url('web_technology');?>">Web Technology</a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('java');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/java.jpg" style="height: 232px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                                <ul class="post-meta">
                                    
                                </ul>
                                <h3 class="entry-title"><a href="<?php echo base_url('java');?>">Java Programming </a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('python');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/python.jpg" style="height: 282px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                                <ul class="post-meta">
                                    
                                </ul>
                                <h3 class="entry-title"><a href="<?php echo base_url('python');?>">Python</a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('net');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/netnew.jpg"  style="height: 282px;"  alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                                <ul class="post-meta">
                                   
                                </ul>
                                <h3 class="entry-title"><a href="<?php echo base_url('net');?>">.NET</a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('selenium_testing');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/selenium.jpg" style="width: 500px;height:279px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                              
                                <h3 class="entry-title"><a href="<?php echo base_url('selenium_testing');?>">Selenium Testing</a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('artificial_intelligence');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/icons/icons/ai_icon.jpg" style="width: 500px;height:279px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                              
                                <h3 class="entry-title"><a href="<?php echo base_url('artificial_intelligence');?>">Artificial Intelliegence</a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('digital_marketing_intern');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/digital.jpg" style="width: 500px;height:279px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                              
                                <h3 class="entry-title"><a href="<?php echo base_url('digital_marketing_intern');?>">Digital Marketing</a></h3>
                               
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('data_science');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/data_science.jpg" style="width: 500px;height:279px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                              
                                <h3 class="entry-title"><a href="<?php echo base_url('data_science');?>">Data Science</a></h3>
                               
                            </div>
                        </article>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-6">
                        <article class="blog-post color-two wow pixFadeLeft" data-wow-delay="0.7s">
                            <div class="feature-image">
                                <a href="<?php echo base_url('aws');?>">
                                    <img src="<?php echo base_url();?>assets/img/courses/icons/icons/aws_icon.jpg" style="width: 500px;height:279px;" alt="blog-thumb">
                                </a>
                            </div>
                            <div class="blog-content">
                              
                                <h3 class="entry-title"><a href="<?php echo base_url('aws');?>">AWS</a></h3>
                               
                            </div>
                        </article>
                    </div>
            </div>
        </section>
        