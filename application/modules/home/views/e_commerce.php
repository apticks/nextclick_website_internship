 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title"> E-Commerce
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li> E-Commerce</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/e_commerce.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About E-Commerce!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Next Click Info Solutions Pvt Ltd, is one of the leading Ecommerce development company in serving clients across the globe. We have unparalleled expertise in Ecommerce website design and development with providing various platforms of CMS (Content Management System) – WordPress, Magento, Joomla-commerce, Open-cart. We provide solutions for any sector who wants to increase the customer database and lead generations.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">Description</h3>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">Our Ecommerce solutions will help transform your business and make your products or services available across the globe. We set up the leading and secure payment gateway with PCI Compliance such as Visa, MasterCard etc. Team Next Click helps you by generating fresh new business in just a few clicks.</p> 
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">A professional ECommerce web design is one of the most critical elements to your online business success. Our job is the make sure that your customers are not just impressed with your site but by the usability o and functionality of your online storefront. It’s not just about the look and feel of your website; it should also be well-organized and easy to navigate. We’ve got several ways to help you design whatever is your budget to get your ecommerce up and running.</p> 
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">Next Click is one of the main ECommerce advancement organization in Hyderabad serving customers over the globe. We are unparalleled mastery in ECommerce web architecture and advancement with giving different stages of CMS (Content Management System) – WordPress, Magento, Joomla-business, Open-truck. We give answers for little and medium endeavors who needs to expand the client database and lead ages.</p> 
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">Our Ecommerce arrangements will help change your business and make your items or administrations accessible over the globe. We set up the main and secure installment portal with PCI Compliance, for example, Visa, MasterCard and so forth. Group Next Click causes you by producing new business in only a couple of snaps.</p> 
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">An expert ECommerce website architecture is a standout amongst the most basic components to your online business achievement. Our activity is the ensure that your clients are inspired with your website as well as by the convenience o and usefulness of your online retail facade. It’s not just about the look and feel of your site; it ought to likewise be efficient and simple to explore. We have a few different ways to enable you to structure whatever is your financial plan to get your internet business fully operational.</p> 
                               
                            </div>

                           </div>
                    </div>
                </div>
            </div>
        </section>
       
       
         
<br>
<br>