     <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Java
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Java</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        
         

          <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                           <img src="<?php echo base_url();?>assets/img/courses/java_internal_image.jpeg" class="elm-one wow pixFade" alt="informes">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Java</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Java is one of the most popular and widely used programming language.Java has been one of
the most popular programming languages for many years.Java is Object Oriented. However it is
not considered as pure object oriented as it provides support for primitive data types (like int,
char, etc)The Java codes are first compiled into bytecode (machine independent code). Then
the byte code is run on Java Virtual Machine (JVM) regardless of the underlying architecture.</p>
 <ul class="list-items wow pixFadeUp" data-wow-delay="0.5s">

                                <li>Core Java</li>
                                <li>ADVANCED JAVA</li>
                                <li>SERVLETS</li>
                                 <li>JSP</li>
                            </ul><a href="<?php echo base_url('contact');?>" class="pix-btn btn-outline-two wow pixFadeUp" data-wow-delay="0.5s">Hurry Up!!!</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp">Description</h3>
                               
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.4s">Java is used in all kind of applications like Mobile Applications (Android is Java based), desktop
applications, web applications, client server applications, enterprise applications and many
more.When compared with C++, Java codes are generally more maintainable because Java does
not allow many things which may lead bad/inefficient programming if used incorrectly. For
example, non-primitives are always references in Java. So we cannot pass large objects (like we
can do in C++) to functions, we always pass references in Java. 
</p> 
<div class="row">
 <div class="col-lg-3">
 <h3 class="sub-title wow pixFadeUp">Core Java Content</h3>
 <li>Introduction to java</li>
                        <li>Definition of Software</li>
                        <li>Definition of program</li>
                        <li>History of java</li>
                        <li>Features of java</li>
                        <li>Operators in java<</li>
<li>etc,etc...</li>
</div>
<div class="col-lg-3">
<h3 class="sub-title wow pixFadeUp">ADVANCED JAVA CONTENT</h3>
 <li>Java JDBC</li>
                        <li>JDBC Introduction</li>
                        <li>Types of JDBC Driver</li>
                        <li>DB Connectivity Steps </li>
                        <li>Creating User with DB</li>
                        <li>Operators in java<</li>
<li>etc,etc...</li>
</div>
<div class="col-lg-3">
     <h3 class="sub-title wow pixFadeUp">SERVLETS CONTENT</h3>
 <li>Introduction to Servlet</li>
                        <li> Definition of Servlet </li>
                        <li>Installation of Tomcat Server</li>
                        <li> Directory Structure of Tomcat</li>
                        <li>Life cycle phases of
servlet</li>
                       
<li>etc,etc...</li>
    </div>
    <div class="col-lg-3">
         <h3 class="sub-title wow pixFadeUp">JSP CONTENT</h3>
 <li>Jsp Introduction</li>
                        <li>Life cycle of Jsp Works</li>
                        <li>Jsp API</li>
                        <li>Jsp Programs</li>
                        <li>Implicit Objects in Jsp</li>
                        <li>Scripting Elements</li>
<li>etc,etc...</li>
    </div>
</div>

                           </div>
                    </div>
                </div>
            </div>
        </section>