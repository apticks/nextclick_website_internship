 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Technology Consulting
</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Technology Consulting</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/technology.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About Technology Consulting!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Every year, your clients want more for less. Controlling costs while providing higher-quality service requires innovative changes to the way you do business. NextClick senior consulting professionals have the real-world experience to advise and support you through these changes and help your team thrive in the current legal environment.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp">Our consulting practice is built on the real-world operational experience of our transformation consultants and subject-matter experts. The support they offer includes:
</h3>
                               
                            </div>
                            <li>Develop a business case for change – We apply our proprietary tools to your legal and business processes to identify opportunities to improve productivity, quality, and your client service delivery.</li>
                            <li>Define service levels, metrics and governance – We’ll help establish your service-level requirements, baseline metrics, and the governance processes that ensure uninterrupted, high-quality service.</li>
                            <li>Design and implement new operating models – Together, we’ll design a solution that meets both your goals and your comfort level with change.</li>
                            <li>Apply technology, best practices, and flexible resources – We’ve done his before and will work with you to apply the right combination of our tools, our experience and our scalable account teams to address your challenges.</li>
                            <li>Plan and manage the change – Before we implement anything, we’ll work with you to communicate coming changes to your staff and roll them out effectively. If staff are reduced, we have experience managing transitions sensitively.</li>

                       

                           </div>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <br>