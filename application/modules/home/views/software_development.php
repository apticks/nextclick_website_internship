 <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title"> Software Development</h1>
                    <ul class="bradcurmed">
                        <li><a href="<?php echo base_url('home');?>" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Software Development</h1></li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
         <section class="about-two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
                            <img src="<?php echo base_url();?>assets/img/courses/software_dev.jpg" alt="about">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content-two">
                            <div class="section-title">
                                <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Its About Software Development!</h2>
                            </div>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">Next Click is a software development company that has enhanced in delivering quality and productive software development services. We make sure to provide excellent services by creating business strategies for our clients and help them to develop the best software at their choice.</p>
                            <p class="description wow pixFadeUp" data-wow-delay="0.5s">With several clients we have managed to help a lot of companies in the past to achieve their objectives, our well-managed group of experts and designers can give the right solutions and technical innovative services for software development.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-content">
                            <div class="section-title">
                                <h3 class="sub-title wow pixFadeUp text-center">More About Software Development</h3>
                                </div>
                                  <p class="description wow pixFadeUp" data-wow-delay="0.4s">Designing the product is another aspect and it does require experience to choose the right tool for the right purpose. It is highly important to build and develop legitimate software for your company, and we serve best software Development Company at Next Click focus on creativity brand building methods and services without sacrificing quality for our client. This is the reason why we try to make the best software for our clients so that they do not have to worry about unsolicited software taking over their business.</p> 
                                <p class="description wow pixFadeUp" data-wow-delay="0.4s">It is highly important that we build web software that is stable and the service which we provide is uninterrupted. We at Next Click being progressive with Top 10 Software Development Company in market, ensure that we build such software for our clients and when we are 100% sure that the result is perfect, then only we hand over the products to our clients. At Next Click, the team scrutinizes each of these aspects so that you can have a solid product in your hand which meets your entire requirement.</p> 
                                 
                               
                            

                           </div>
                    </div>
                </div>
            </div>
        </section>
        
       
         
<br>
<br>