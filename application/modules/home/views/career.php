     <section class="page-banner">
            <div class="container">
                <div class="page-title-wrapper">
                    <h1 class="page-title">Career</h1>
                    <ul class="bradcurmed">
                        <li><a href="index.html" rel="noopener noreferrer">Home</a>
                        </li>
                        <li>Career</li>
                    </ul>
                </div>
            </div>
            <svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>
            <ul class="animate-ball">
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
                <li class="ball"></li>
            </ul>
        </section>
        <section class="portfolio-single">
            <div class="container">
                <div class="portfolio-single-wrapper">
                    <div class="port-header">
                        <div class="portfolio-title">
                            <h2 class="title">
</h2>
                        </div>
                        <div class="share-link"><span class="share">Share:</span>
                            <ul class="footer-social-link">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                </li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="portfolio-content">
                        <div class="feature-image">
                            <img src="media/portfolio/30.jpg" alt="portfolio">
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="content-inner">
                                    <h3 class="sub-title">Welcome to the place, where your ideas gets accomplished!!
Team NextKlick.com</h3>
                                    <p>Search for openings and apply for positions at NextKlick.com. We strive to build a rewarding environment that supports the creativity, collaboration and performance of our associates. 
On provision of much-needed exposure that a fresh trainee could seek, we use leading-edge technologies and strategies to revive the way every job is done. So, join our disparate group of innovators working together to solve industries & organisations most challenging problems.
</p>
                                  
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="portfolio-info">
                                    <ul class="info">
                                        <li>Date <span>March 24, 2019</span>
                                        </li>
                                        <li>Client Name <span>Lance Bogrol, New York</span>
                                        </li>
                                        <li>Project Type <span>Digital Design, Suke Agency</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="portfolio-nav">
                        <li class="prev"><i class="ei ei-arrow_left"></i>  <a href="#">Previous</a>
                        </li>
                        <li class="next"><a href="#">Next</a>  <i class="ei ei-arrow_right"></i>
                        </li>
                    </ul>
                    <div class="faq-section-two">
                <div class="container">
                    <div class="section-title text-center">
                        <h3 class="sub-title wow pixFadeUp">Frequently ask Question</h3>
                        <h2 class="title wow pixFadeUp">Want to ask something from us?</h2>
                    </div>
                    <div class="tabs-wrapper wow pixFadeUp" data-wow-delay="0.5s">
                        <ul class="nav faq-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="design-tab" data-toggle="tab" href="#design" role="tab" aria-controls="design" aria-selected="true">UI/UX Design</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="service-tab" data-toggle="tab" href="#service" role="tab" aria-controls="service" aria-selected="false">Service</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="false">General</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="branding-tab" data-toggle="tab" href="#branding" role="tab" aria-controls="branding" aria-selected="false">Branding</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="design" role="tabpanel" aria-labelledby="design-tab">
                                <div id="accordion" class="faq faq-two pixFade">
                                    <div class="card active">
                                        <div class="card-header" id="heading100">
                                            <h5><button class="btn btn-link" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">What do you want to do?</button></h5>
                                        </div>
                                        <div id="collapse001" class="collapse show" aria-labelledby="heading100" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                <p>Easy peasy owt to do with me cras I don't want no agro what a load of rubbish starkers absolutely bladdered, old tinkety tonk old fruit so I said the full monty.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="heading200">
                                            <h5><button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse100" aria-expanded="true" aria-controls="collapse100">Any relevant skills/experience you would like to share?</button></h5>
                                        </div>
                                        <div id="collapse100" class="collapse" aria-labelledby="heading200" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                <li>Trainer</li>
                                                <li>Career counsellor   </li>
                                                <li>Administration</li>
                                                <li>Accounting </li>
                                            </div>
                                        </div>
                                    </div>
                                   
                                  
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="button-container text-center mt-40"><a href="#" class="pix-btn btn-outline wow pixFadeUp">Explore Forum</a>
                    </div>
                </div>
            </div>
                </div>
            </div>
        </section>