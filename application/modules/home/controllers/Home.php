<?php

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/master/main';
        $this->load->model('contact_model');
        $this->load->model('portfolio_details_model');
         $this->load->model('Subscribe_model');
    }
    
    public function index(){
        $this->data['title'] = 'home';
        $this->data['content'] = 'home/home';
        $this->_render_page($this->template, $this->data);
    }
     public function about_us(){
        $this->data['title'] = 'about_us';
        $this->data['content'] = 'home/about_us';
        $this->_render_page($this->template, $this->data);
    }
    public function career(){
        $this->data['title'] = 'career';
        $this->data['content'] = 'home/career';
        $this->_render_page($this->template, $this->data);
    }

     public function contact()
    {
        $this->form_validation->set_rules($this->contact_model->rules);

        if ($this->form_validation->run() === false) {
            $this->data['title'] = 'Contact Us';
            $this->data['content'] = 'home/contact';
            $this->_render_page($this->template, $this->data);
        } else {

            
            $id = $this->contact_model->insert([
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'subject' => $this->input->post('subject'),
                'mobile' => $this->input->post('mobile'),
                'message' => $this->input->post('message')
            ]);
            if (! empty($id))
                $this->session->set_flashdata('success', 'Contact Details Submitted Successfully..!');
            else
                $this->session->set_flashdata('error', 'Something went wrong..!');

            redirect('contact', 'refresh');
        }
    }
    public function privacy(){
        $this->data['title'] = 'privacy';
        $this->data['content'] = 'home/privacy';
        $this->_render_page($this->template, $this->data);
    }
  
     
   public function subscribe()
    {
        $id = $this->Subscribe_model->insert([
            'email' => $this->input->post('email')
        ]);
        if (! empty($id))
            $this->session->set_flashdata('subscribed', 'Successfully Subscribed');
        else
            $this->session->set_flashdata('error', 'Something went wrong..!');
        redirect('home', 'refresh');
    }
    
    /**Internship*/
    public function python(){
        $this->data['title'] = 'python';
        $this->data['content'] = 'home/python';
        $this->_render_page($this->template, $this->data);
    }
    public function machine_learning(){
        $this->data['title'] = 'machine_learning';
        $this->data['content'] = 'home/machine_learning';
        $this->_render_page($this->template, $this->data);
    }
    public function java(){
        $this->data['title'] = 'java';
        $this->data['content'] = 'home/java';
        $this->_render_page($this->template, $this->data);
    }
    public function web_technology(){
        $this->data['title'] = 'web_technology';
        $this->data['content'] = 'home/web_technology';
        $this->_render_page($this->template, $this->data);
    }
    public function net(){
        $this->data['title'] = '.net';
        $this->data['content'] = 'home/net';
        $this->_render_page($this->template, $this->data);
    }
      public function selenium_testing(){
        $this->data['title'] = 'selenium_testing';
        $this->data['content'] = 'home/selenium_testing';
        $this->_render_page($this->template, $this->data);
    }
      public function artificial_intelligence(){
        $this->data['title'] = 'artificial_intelligence';
        $this->data['content'] = 'home/artificial_intelligence';
        $this->_render_page($this->template, $this->data);
    }
     public function digital_marketing_intern(){
        $this->data['title'] = 'digital_marketing_intern';
        $this->data['content'] = 'home/digital_marketing_intern';
        $this->_render_page($this->template, $this->data);
    }
    public function data_science(){
        $this->data['title'] = 'data_science';
        $this->data['content'] = 'home/data_science';
        $this->_render_page($this->template, $this->data);
    }
     public function aws(){
        $this->data['title'] = 'aws';
        $this->data['content'] = 'home/aws';
        $this->_render_page($this->template, $this->data);
    }


      /**Services*/

       public function mobile_development(){
        $this->data['title'] = 'mobile_development';
        $this->data['content'] = 'home/mobile_development';
        $this->_render_page($this->template, $this->data);
    }
     public function testing_quality_assurance(){
        $this->data['title'] = 'testing_quality_assurance';
        $this->data['content'] = 'home/testing_quality_assurance';
        $this->_render_page($this->template, $this->data);
    }
     public function technology_consulting(){
        $this->data['title'] = 'technology_consulting';
        $this->data['content'] = 'home/technology_consulting';
        $this->_render_page($this->template, $this->data);
    }
     public function resource_fulfillment(){
        $this->data['title'] = 'resource_fulfillment';
        $this->data['content'] = 'home/resource_fulfillment';
        $this->_render_page($this->template, $this->data);
    }
     public function website_design(){
        $this->data['title'] = 'website_design';
        $this->data['content'] = 'home/website_design';
        $this->_render_page($this->template, $this->data);
    }
    public function e_commerce(){
        $this->data['title'] = 'e_commerce';
        $this->data['content'] = 'home/e_commerce';
        $this->_render_page($this->template, $this->data);
    }
     public function digital_marketing(){
        $this->data['title'] = 'digital_marketing';
        $this->data['content'] = 'home/digital_marketing';
        $this->_render_page($this->template, $this->data);
    }
     public function software_development(){
        $this->data['title'] = 'software_development';
        $this->data['content'] = 'home/software_development';
        $this->_render_page($this->template, $this->data);
    }
     public function web_development(){
        $this->data['title'] = 'web_development';
        $this->data['content'] = 'home/web_development';
        $this->_render_page($this->template, $this->data);
    }

   
}

