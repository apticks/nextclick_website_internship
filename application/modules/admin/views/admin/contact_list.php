<div class="row">
	<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of  Contacts</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Name</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Subject</th>
									<th>Message</th>
									<th>Actions</th>

								</tr>
							</thead>
							
							<tbody>
								<?php if(!empty($contact)):?>
    							<?php $sno = 1; foreach ($contact as $con):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $con['name'];?></td>
    									<td><?php echo $con['email'];?></td>
    									<td><?php echo $con['mobile'];?></td>
    									<td><?php echo $con['subject'];?></td>
    									<td><?php echo $con['message'];?></td>
    									
    									<td>
    										<!-- <a href="<?php echo base_url()?>admin/contact/view?id=<?php echo $con['id'];?>" class="mr-2  text-success">
                                                            <i class="fas fa-eye"></i>
                                                        </a> -->
                                              <a href="<?php echo base_url()?>admin/contact/view?id=<?php echo $con['id'];?>" 
							class="btn btn-primary mt-27 ">View</a>          
    									</a></td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='10'><h3><center>No Contacts</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
	</div>