-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2020 at 09:22 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `temp`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `message` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=deleted,1=Active,2=Inative',
  `created_user_id` bigint(20) NOT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `last_id` bigint(20) NOT NULL DEFAULT 100,
  `priority` smallint(6) NOT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = deleted, 1 = Active, 2 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `code`, `last_id`, `priority`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'admin', 'Admin', 'NCA', 107, 1, NULL, NULL, '0000-00-00 00:00:00', '2019-11-02 12:49:48', NULL, 1),
(18, 'member', 'member', 'SDSN', 16, 0, NULL, NULL, '2019-11-11 18:33:37', '2019-11-18 14:14:51', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups_permissions`
--

CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(4) DEFAULT 0 COMMENT '0 = Deny, 1 = Allow',
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `perm_key` varchar(30) NOT NULL,
  `perm_name` varchar(100) NOT NULL,
  `parent_status` varchar(100) DEFAULT 'parent',
  `description` longtext NOT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = deleted, 1 = Active, 2 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_details`
--

CREATE TABLE `portfolio_details` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `client` varchar(25) NOT NULL,
  `project_date` date NOT NULL,
  `project_category` varchar(25) NOT NULL,
  `desc` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `portfolio_details`
--

INSERT INTO `portfolio_details` (`id`, `title`, `client`, `project_date`, `project_category`, `desc`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'Dialmama', 'Mr.Dialmama', '2020-08-22', 'Android App and Web App', '<p>An&nbsp;<strong>ecommerce</strong>&nbsp;mobile&nbsp;<strong>app</strong>&nbsp;should aim at providing an immersive and unique experience that makes online shopping a much simpler and seamless process. At the same time, it should include features that drive sales and boost revenues for your business.</p>\r\n', '2020-08-29 15:40:22', NULL, NULL, 1),
(2, 'NextClick', 'Mr.VinaySagar', '2020-08-25', 'Android App and Web App', '<p>An&nbsp;<strong>ecommerce</strong>&nbsp;mobile&nbsp;<strong>app</strong>&nbsp;should aim at providing an immersive and unique experience that makes online shopping a much simpler and seamless process. At the same time, it should include features that drive sales and boost revenues for your business.</p>\r\n', '2020-08-29 15:41:01', NULL, '2020-08-29 15:44:02', 1),
(3, 'TradeBea', 'Mr.YashRaj Sahu', '2020-08-26', 'Android App and Web App', '<p>An&nbsp;<strong>ecommerce</strong>&nbsp;mobile&nbsp;<strong>app</strong>&nbsp;should aim at providing an immersive and unique experience that makes online shopping a much simpler and seamless process. At the same time, it should include features that drive sales and boost revenues for your business.</p>\r\n', '2020-08-29 15:41:40', NULL, NULL, 1),
(4, 'Limpex', 'Mr.Ravi Teja', '2020-08-30', 'Web App', '<h1>&nbsp;</h1>\r\n\r\n<p>Agri&nbsp;<strong>App</strong>&nbsp;is one of the most liked&nbsp;<strong>apps</strong>&nbsp;by farmers. ... This android application provides information about the latest&nbsp;<strong>agriculture</strong>&nbsp;advice, latest mandi prices, and various&nbsp;<strong>farming</strong>&nbsp;tips. It also provides weather forecast information. It also provides&nbsp;<strong>agriculture</strong>&nbsp;alerts to farmers in 10 Indian languages.</p>\r\n', '2020-08-29 15:42:42', NULL, NULL, 1),
(5, 'NextClick', 'Mr.VinaySagar', '2020-08-30', 'Android App and Web App', '<p>An&nbsp;<strong>ecommerce</strong>&nbsp;mobile&nbsp;<strong>app</strong>&nbsp;should aim at providing an immersive and unique experience that makes online shopping a much simpler and seamless process. At the same time, it should include features that drive sales and boost revenues for your business.</p>\r\n', '2020-08-29 15:45:01', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `key` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_user_id` bigint(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_user_id` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0-Deleted 1-Active 2-Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 'system_name', 'Next Click', 0, '2019-04-30 15:41:07', 1, '2019-10-18 06:15:28', NULL, 1),
(2, 'system_title', 'Your success begins here', 0, '2019-04-30 15:41:07', 1, '2019-10-18 06:15:28', NULL, 1),
(3, 'address', 'Hyderabad', 0, '2019-04-30 15:41:07', 1, '2019-10-18 06:15:28', NULL, 1),
(4, 'mobile', '9704100111', 0, '2019-04-30 15:41:07', 1, '2019-10-18 06:15:28', NULL, 1),
(5, 'system_email', 'info@gmail.com', 0, '2019-04-30 15:41:07', 1, '2019-10-17 10:36:40', NULL, 1),
(6, 'email_password', '123123', 0, '2019-04-30 15:41:07', 1, '2019-10-17 10:36:40', NULL, 1),
(7, 'sms_username', '123', 0, '2019-04-30 15:41:07', 1, '2019-10-17 10:22:46', NULL, 1),
(8, 'sms_sender', 'TRAI', 0, '2019-04-30 15:41:07', 1, '2019-10-17 10:22:46', NULL, 1),
(9, 'sms_hash', 'HASH', 0, '2019-04-30 15:41:07', 1, '2019-10-17 10:22:46', NULL, 1),
(10, 'privacy', '<h1 style=\"text-align: center;\"><span style=\"color:#008000\"><u><span style=\"font-size:22px\"><span style=\"font-family:lucida sans unicode,lucida grande,sans-serif\"><var><strong><em>MyPulse</em></strong></var></span></span></u></span></h1>\r\n', 0, '2019-04-30 15:41:07', NULL, '0000-00-00 00:00:00', NULL, 1),
(11, 'terms', '<p>hi this is for testing</p>\r\n', 0, '2019-04-30 15:41:07', NULL, '0000-00-00 00:00:00', NULL, 1),
(12, 'facebook', 'https://www.facebook.com/', 0, '2019-07-22 14:05:08', 1, '2019-10-18 06:15:28', NULL, 1),
(13, 'twiter', 'https://www.twiter.com/', 0, '2019-07-22 14:05:08', 1, '2019-10-18 06:15:28', NULL, 1),
(14, 'youtube', 'https://www.youtube.com/', 0, '2019-07-22 14:05:08', 1, '2019-10-18 06:15:28', NULL, 1),
(15, 'skype', 'http://skype.com/', 0, '2019-10-16 09:49:33', 1, '2019-10-18 06:15:28', NULL, 1),
(16, 'pinterest', 'http://pinterest.com/', 0, '2019-10-16 09:50:50', 1, '2019-10-18 06:15:28', NULL, 1),
(17, 'smtp_port', '587', 0, '2019-10-16 11:06:00', 1, '2019-10-18 10:46:48', NULL, 1),
(18, 'smtp_host', 'mail.akshayacomputers.com', 0, '2019-10-16 11:06:00', 1, '2019-10-18 10:46:48', NULL, 1),
(19, 'smtp_username', 'info@akshayacomputers.com', 0, '2019-10-16 11:06:42', 1, '2019-10-18 10:46:48', NULL, 1),
(20, 'smtp_password', 'akshaya@123', 0, '2019-10-16 11:06:42', 1, '2019-10-18 10:46:48', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `unique_id` varchar(50) NOT NULL,
  `wallet` decimal(14,2) NOT NULL DEFAULT 0.00,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `list_id` bigint(20) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `updated_user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 = deleted, 1 = Active, 2 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `unique_id`, `wallet`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `list_id`, `first_name`, `last_name`, `company`, `phone`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, '192.168.1.28', 'administrator', 'NCA0101', '1000.00', '$2y$08$WMQkCQBAstJMzuXI8fUlP.MPawjm9m6fkc4basz.sR1RrIK9jCeUm', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1598707986, 1, 0, 'Admin', 'istrator', 'ADMIN', '7013907291', NULL, NULL, '2019-09-20 16:59:05', '2019-11-21 13:43:01', NULL, 1),
(52, '::1', 'tptrupti011@gmail.com', 'SDSN0006', '0.00', '$2y$08$cE1RtXxAZcg5Y7pY1e16PeAxViHjM2XbC.Qr0kKmhVapVEBdiS52u', NULL, 'tptrupti011@gmail.com', 'd92a8ade4ba6db781eb2150fc2145b69b597cba3', NULL, NULL, NULL, 1573554582, NULL, 0, 0, 'mehar', NULL, NULL, NULL, NULL, NULL, '2019-11-12 15:59:42', NULL, NULL, 1),
(54, '::1', 'meharkotii@gmail.com', 'SDSN0008', '0.00', '$2y$08$FIqjXJXTiatrUKJ4WLaBve.ASM0tHOVB2Fnb8QESMeKG37R.9NPM6', NULL, 'meharkotii@gmail.com', '4bcf240218fe9cfd9b7eb502afebbefb713c1622', NULL, NULL, NULL, 1573644068, NULL, 0, 0, 'mehar koti', NULL, NULL, NULL, NULL, NULL, '2019-11-13 16:51:08', NULL, NULL, 1),
(55, '::1', 'meharkotiiiw@gmail.com', 'SDSN0009', '0.00', '$2y$08$Ah5/3e07rTbWiuu.OXiiaeYoMX84zgFIBgiJmUq7EfsCWVyDnsG5i', NULL, 'meharkotiiiw@gmail.com', '845ac22043e753bed96088da58281e44739975da', NULL, NULL, NULL, 1573644100, NULL, 0, 0, 'mehar koti', NULL, NULL, NULL, NULL, NULL, '2019-11-13 16:51:40', NULL, NULL, 1),
(58, '::1', 'mehargrepthor@gmail.com', 'SDSN0014', '0.00', '$2y$08$9WrnlHjSp182RizqJEzf0eNQYlGwL2aZyfnpfqJhHBZNxV02Ur1PK', NULL, 'mehargrepthor@gmail.com', 'ad42d4551faae5ac601ba7499ce50293fb91b150', NULL, NULL, NULL, 1574066922, NULL, 0, 0, 'mehar koti', NULL, NULL, NULL, NULL, NULL, '2019-11-18 14:18:42', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 1, 1),
(50, 52, 18),
(52, 54, 18),
(53, 55, 18),
(56, 58, 18);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = Deny, 1 = Allow',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `user_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 19, 57, 1, 0, 0),
(4, 20, 62, 1, 0, 0),
(5, 21, 62, 1, 0, 0),
(7, 30, 62, 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`group_id`,`perm_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permKey` (`perm_key`);

--
-- Indexes for table `portfolio_details`
--
ALTER TABLE `portfolio_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`user_id`,`perm_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `portfolio_details`
--
ALTER TABLE `portfolio_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
